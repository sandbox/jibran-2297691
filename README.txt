
-- SUMMARY --

The Webform Submissions Prune module allows you to remove webform submissions
after a given time period.

For a full description of the module, visit the project page:
  http://drupal.org/node/2297691

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/2297691


-- REQUIREMENTS --

webform.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- USAGE --

* After enabling the module just visit module config page at
  admin/config/content/webform-prune.

* Add node title of the webform.

* Configure the interval and save the form.
