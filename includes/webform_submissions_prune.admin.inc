<?php

/**
 * @file
 * Admin callbacks for webform submissions prune module.
 */

/**
 * Admin form constructor for webform prune module.
 */
function webform_submissions_prune_admin_form($form, &$form_state) {
  $titles = array();
  if ($webforms = db_query('SELECT * FROM {webform_submissions_prune}')->fetchAllKeyed()) {
    $titles = db_query('SELECT nid, title FROM {node} WHERE nid IN (:nid)', array(':nid' => array_keys($webforms)))->fetchAllKeyed();
  }
  $select = array(
    '#type' => 'select',
    '#options' => webform_submissions_prune_time_options(),
  );

  $form = array(
    '#tree' => TRUE,
    '#webforms' => $webforms,
  );

  $form['webforms'] = array();
  foreach ($titles as $nid => $title) {

    $form['webforms'][$nid]['nid'] = array(
      '#type' => 'hidden',
      '#default_value' => $nid,
    );

    $form['webforms'][$nid]['title'] = array(
      '#markup' => check_plain($title),
    );

    $form['webforms'][$nid]['interval'] = $select + array(
      '#default_value' => $webforms[$nid],
    );

    $form['webforms'][$nid]['remove'] = array(
      '#type' => 'link',
      '#title' => t('Remove'),
      '#href' => 'admin/config/content/webform-prune/remove/' . $nid,
    );
  }

  $form['add'] = array();

  $form['add']['nid'] = array(
    '#type' => 'textfield',
    '#size' => 50,
    '#maxlength' => 255,
    '#autocomplete_path' => 'webform-submissions-prune/autocomplete',
  );

  $form['add']['add'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#weight' => 45,
    '#validate' => array('webform_submissions_prune_add_validate'),
    '#submit' => array('webform_submissions_prune_add_submit'),
  );

  $form['actions'] = array('#type' => 'actions');

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );
  // @todo Add prune now functionality.
  //   $form['actions']['prune'] = array(
  //   '#type' => 'submit',
  //   '#value' => t('Prune Now'),
  //   '#submit' => array('webform_submissions_prune_now_submit'),
  // );

  return $form;
}

/**
 * Admin form submit for webform prune module.
 */
function webform_submissions_prune_admin_form_submit($form, &$form_state) {
  $values = $form_state['values'];
  $webforms = $form['#webforms'];
  if (isset($values['webforms']) ) {
    foreach ($values['webforms'] as $nid => $data) {
      $already = in_array($nid, array_keys($webforms));
      // Insert if it's a new nid and update interval for an existing nid.
      if (!$already) {
        db_insert('webform_submissions_prune')
          ->fields(array('nid', 'interval'))
          ->values(array($nid, $data['interval']))
          ->execute();
      }
      elseif ($already && $webforms[$nid] != $data['interval']) {
        db_update('webform_submissions_prune')
          ->condition('nid', $nid)
          ->fields(array(
            'interval' => $data['interval'],
          ))
          ->execute();
      }
    }
  }
}

/**
 * Validates add a new webform element of webform_submissions_prune_admin_form.
 */
function webform_submissions_prune_add_validate($form, &$form_state) {

  $value = $form_state['values']['add']['nid'];
  $webforms = $form['#webforms'];
  $nid = NULL;

  if (!empty($value)) {
    // Check whether we have an explicit "[nid:n]" input.
    preg_match('/^(?:\s*|(.*) )?\[\s*nid\s*:\s*(\d+)\s*\]$/', $value, $matches);
    if (!empty($matches)) {
      list(, $title, $nid) = $matches;
      if (!empty($nid) && !db_query('SELECT nid FROM {webform} WHERE nid = :nid', array(':nid' => $nid))->fetchField()) {
        form_error($form['add']['nid'], t('This is not a valid webform. Please check your selection.'));
      }
      if (in_array($nid, array_keys($webforms))) {
        form_error($form['add']['nid'], t('Already in the list.'));
      }
      // Explicit nid. Check that the 'title' part matches the actual title for
      // the nid.
      if (!empty($title)) {
        $real_title = db_query('SELECT title FROM {node} WHERE nid = :nid', array(':nid' => $nid))->fetchField();
        if (trim($title) != trim($real_title)) {
          form_error($form['add']['nid'], t('This is not a valid webform tite. Please check your selection.'));
        }
      }
    }
    else {
      // No explicit nid (the submitted value was not populated by autocomplete
      // selection). Get the nid of a referencable webform from the entered
      // title.
      if ($titles = webform_submissions_prune_autocomplete_helper($value)) {
        // @todo The best thing would be to present the user with an
        // additional form, allowing the user to choose between valid
        // candidates with the same title. ATM, we pick the first
        // matching candidate...
        $nid = key($titles);
      }
      else {
        form_error($form['add']['nid'], t('Found no valid webform with that title.'));
      }
    }
  }

  // Set the element's value as the node id that was extracted from the entered
  // input.
  form_set_value($form['add']['nid'], $nid, $form_state);
}

/**
 * Submits adds a new webform element for webform_submissions_prune_admin_form.
 */
function webform_submissions_prune_add_submit($form, &$form_state) {
  if ($nid = $form_state['values']['add']['nid']) {
    db_insert('webform_submissions_prune')
      ->fields(array('nid'))
      ->values(array($nid))
      ->execute();
  }
  $form_state['redirect'] = array('admin/config/content/webform-prune');
}

/**
 * Form constructor for the webform_submissions_prune_admin_form config deletion
 * confirmation form.
 */
function webform_submissions_prune_delete_confirm($form, &$form_state, $node) {
  $form['#node'] = $node;
  // Always provide entity id in the same form key as in the entity edit form.
  $form['nid'] = array('#type' => 'value', '#value' => $node->nid);
  return confirm_form($form,
    t('Are you sure you want to remove this webform %title form the table?', array('%title' => $node->title)),
    'admin/config/content/webform-prune',
    t('This action cannot be undone.'),
    t('Remove'),
    t('Cancel')
  );
}

/**
 * Submit function for webform_submissions_prune_delete_confirm().
 */
function webform_submissions_prune_delete_confirm_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $node = node_load($form_state['values']['nid']);
    db_delete('webform_submissions_prune')
      ->condition('nid', $node->nid)
      ->execute();
    drupal_set_message(t('Webform %title is removed form the table', array('%title' => $node->title)));
  }

  $form_state['redirect'] = 'admin/config/content/webform-prune';
}

/**
 * Theme the webform submissions prune admin form as table.
 */
function theme_webform_submissions_prune_admin_form($variables) {
  $form = $variables['form'];

  $webforms = $form['#webforms'];

  $header = array(t('Title'), t('Interval'), t('Operations'));
  $rows = array();

  if (!empty($webforms)) {
    foreach (array_keys($webforms) as $nid) {
      $row = array(
        drupal_render($form['webforms'][$nid]['title']),
        drupal_render($form['webforms'][$nid]['interval']),
        drupal_render($form['webforms'][$nid]['remove']),
      );
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No webform added yet.'), 'colspan' => 3));
  }
  // Append the add form.
  $rows[] = array(
    array('colspan' => 2, 'data' => drupal_render($form['add']['nid'])),
    drupal_render($form['add']['add']),
  );

  $output = '';
  $output .= theme('table', array('header' => $header, 'rows' => $rows));
  $output .= drupal_render_children($form);
  return $output;
}
